package com.example.dao;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

//リポジトリー
@Repository
public class HelloRepository {

//Autowired setter,getterが使えるようになる インスタンスなしで使える
@Autowired
private JdbcTemplate jdbcTemplate;

public Map<String,Object>findOne(int id){

	//SELECT文
	String query = "SELECT"
			+"employee_id,"
			+"employee_name,"
			+"age "
			+"FROM employee"
			+"WHERE employee_id = ?";

	//検索実行
	//jdbcTemplateを使ってSELECT文の実行
	Map<String,Object>employee = jdbcTemplate.queryForMap(query, id);

	return employee;
}
}
