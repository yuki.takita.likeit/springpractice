package com.example.web;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.dto.Employee;
import com.example.service.HelloService;

//サーブレットのコントローラーとして機能させる
@Controller
public class HelloController {

	//HTTPのget処理ができるようになる
		//localhost:8080/helloへのgetリクエスト
		//@GetMapping(画面遷移先)

	//getとpostの違い
		//getはURLの後ろに値を追加する = ページ遷移
		//poatはHTTPSのメッセージ内に値を格納して渡す


		//大体インスタンスの作成と同じ
	@Autowired
	private HelloService helloService;

	//Getで画面遷移
	@GetMapping("/hello")
	public String getHello() {
		//hello.htmlに画面遷移
		return "hello";
	}

	//Postで値を受け取る
	@PostMapping("/hello")
	public String postRequest(@RequestParam("text1")String str, Model model) {
	//RequestParamでname属性指定で値を受け取る

		//画面から受け取った文字をmodelに登録
		//キーと値のセット
		model.addAttribute("sample",str);

		//helloResponse.htmlに画面遷移
		return "helloResponse";
	}

	@PostMapping("/hello/db")
	public String postDbRequest(@RequestParam("text2")String str,Model model) {
	//RequestParamでname属性指定で値を受け取ってstrに格納
		//Stringからint型に変換
		int id = Integer.parseInt(str);

		//1件検索
		Employee employee = helloService.findOne(id);

		//検索結果をModelに登録
		model.addAttribute("id",employee.getEmployeeId());
		model.addAttribute("name", employee.getEmployeeName());
		model.addAttribute("age",employee.getAge());

		//helloResponseDB.htmlに画面遷移
		return "helloResponseDB";


	}
}
